/*
 * Sergey Svetlov, 2019
 * JSC "InfoTeCS", Moscow, Russia  
 */

#ifndef EXPERIMENT_H_
#define EXPERIMENT_H_

#include <list>

#include "lsx.h"
#include "sbox.h"
#include "generation.h"
#include "common.h"


class Experiment
{
 public:
  Experiment(const LSX& cipher, const ByteVector& masterKey);
  ~Experiment() = default;
  void Run();

protected:
  void ShowParams();

  ByteVector restoreRoundKey1(const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates,
                        const ByteVector& plainText);
  ByteVector restoreRoundKey2(const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates,
                        const ByteVector& plainText, const ByteVector& rk1);

  std::pair<Difference, Difference> stage1(
                std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates, const ByteVector& plainText);

  std::pair<Difference, Difference> stage2(
                std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates, const ByteVector& plainText);
                
  std::list<std::pair<Difference, Difference>> stage3(
                std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates, const ByteVector& plainText);

private:
  LSX cipher_;
  const ByteVector masterKey_;
};

#endif /* EXPERIMENT_H_ */
