/*
 * common.h
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <math.h>

#include "commonTypes.h"
#include "containerConcept.h"
#include "difference.h"
#include "experimentTypes.h"

void Show(const Byte2Vector &a);
void Show(const ByteVector &a);
void Show(const Difference::Data &d);
void Show(const DifferenceColumn& a);

template <Container container_t>
void Show(const container_t &a, const char* prefix="")
{
  std::cout << prefix;
  for (size_t i = 0; i < a.size(); i++)
  {
    std::cout << std::hex << std::uppercase << std::setfill('0') << std::setw(2)
              << static_cast<int>(a[i]) << ' ';
  }
  std::cout << std::endl;
}

const ByteVector operator^(const ByteVector& left, const ByteVector& right);
const DifferenceColumn operator^(const DifferenceColumn& left, const DifferenceColumn& right);

uint8_t mul_GF256(uint8_t x, uint8_t y) noexcept;
size_t roundUp(size_t a, size_t divider) noexcept;

DifferenceColumn extractColumn(const Difference::Data& d, size_t pos);
std::list<uint8_t> splitByBytes(size_t val, size_t count);
#endif /* COMMON_H_ */
