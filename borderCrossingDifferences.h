#ifndef BORDER_CROSSING_DIFFERENCES
#define BORDER_CROSSING_DIFFERENCES

#include <omp.h>

#include "common.h"
#include "difference.h"
#include "experimentTypes.h"
#include "possibleDifferences.h"
#include "lsx.h"

static Difference::Data calcOneDifference(const Difference& diff, size_t diffPos, uint8_t val, const SBox<uint8_t>& sbox);

static OrderedDifferenceColumnContainer extractColumns(size_t diffPos,
            OrderedDifferenceDataContainer::const_iterator it,
            const OrderedDifferenceDataContainer::const_iterator& lastIt);

static std::vector< std::reference_wrapper<Difference::Data> > wrapReference(const OrderedDifferenceDataContainer& src);

class BorderCrossingDifferences {
public:
    BorderCrossingDifferences(size_t depth, const Difference::Data& difference, size_t diffPos, LSX& cipher,
                                const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& context)
        : cipher_(cipher),
            diffPos_( calcNewPos(depth, diffPos) )
    {
        data_ = fill(depth, diffPos, 0, { const_cast<Difference::Data&>(difference) }, context);
    }

    // const Difference::Data& get(size_t index) const
    // {
    //     return data_[index];
    // }

    // const OrderedDifferenceDataContainer& getData() const
    // {
    //     return data_;
    // }
    
    OrderedDifferenceColumnContainer extractColumns() const
    {
        return ::extractColumns(diffPos_, data_.cbegin(), data_.cend());
    }

protected:
    size_t calcNewPos(size_t rounds, size_t diffPos) const
    {
        return (diffPos + rounds * cipher_.get_linear_tacts()) % LSX::state_size_default;
    }

    OrderedDifferenceDataContainer fill(size_t depth, size_t diffPosBasic, size_t currentRound,
                                        const std::vector< std::reference_wrapper<Difference::Data> >& differences,
                                        const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& context)
    {
        OrderedDifferenceDataContainer result;

        const SBox<uint8_t>& sbox = cipher_.get_sbox();
        const size_t diffPos = calcNewPos(currentRound, diffPosBasic);
        
        size_t i;
        #pragma omp parallel for private(i)
        #pragma omp shared(result)
        for (i = 0; i < differences.size(); ++i) {
            Difference::Data& currentDiff = differences[i];
            Difference diff(currentDiff);

            // Устанавливаем все уже известные (ранее вычисленные) столбцы
            for (size_t pos = 0; pos < LSX::state_size_default; ++pos) {
                const size_t actualPos = calcNewPos(currentRound, pos);
                
                if (context[pos].size() > 0) {
                    size_t ind = std::get<0>(context[pos].front());
                    std::list<uint8_t> knownBytes = splitByBytes(ind, depth);
                    auto knownIt = knownBytes.begin();
                    std::advance(knownIt, currentRound);

                    for (size_t j = 0; j < diff.get_size(); ++j)
                        diff.get_row(j)[actualPos] = sbox[ *knownIt ^ currentDiff.data[j][actualPos] ];
                }
            }
            diff.get_anchor() = diff.get_row(0);

            for (size_t j = 0; j < MAX_BYTE; ++j) {
                Difference::Data dd = calcOneDifference(diff, diffPos, j, sbox);
                
                #pragma omp critical
                {
                    dd = applyLinearTransform(std::move(dd));
                    result.insert(j + i * differences.size(), std::move(dd));
                }
            }
        }

        if (depth == currentRound + 1)
            return result;
        else
            return fill(depth, diffPos, currentRound + 1, wrapReference(result), context);
    }

    Difference::Data applyLinearTransform(Difference::Data&& dd) const
    {
        for (size_t i = 0; i < dd.data.size(); ++i) {
            cipher_.get_state() = dd.data[i];
            cipher_.LinearTransform();
            dd.data[i] = cipher_.get_state();
        }

        return dd;
    }

private:
    OrderedDifferenceDataContainer data_;
    LSX& cipher_;
    size_t diffPos_;
};


class InverseBorderCrossingDifferences {
public:
    InverseBorderCrossingDifferences(size_t depth, const Difference::Data& difference, size_t diffPos, LSX& cipher,
                                    const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& context)
        : cipher_(cipher),
            diffPos_( calcNewPos(depth, diffPos) )
    {
        data_ = fill(depth, diffPos, 0, { const_cast<Difference::Data&>(difference) }, context);
    }

    OrderedDifferenceColumnContainer extractColumns() const
    {
        return ::extractColumns(diffPos_, data_.cbegin(), data_.cend());
    }

protected:
    size_t calcNewPos(size_t rounds, size_t diffPos) const
    {
        return (diffPos + (cipher_.get_rounds() - rounds)
                    * cipher_.get_linear_tacts()) % LSX::state_size_default;
    }

    OrderedDifferenceDataContainer fill(size_t depth, size_t diffPosBasic, size_t currentRound,
                        const std::vector< std::reference_wrapper<Difference::Data> >& differences,
                        const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& context)
    {
        OrderedDifferenceDataContainer result;

        const SBox<uint8_t>& sbox = cipher_.get_invsbox();
        const size_t diffPos = calcNewPos(currentRound + 1, diffPosBasic);

        size_t i;
        #pragma omp parallel for private(i)
        #pragma omp shared(result)
        for (i = 0; i < differences.size(); ++i) {
            Difference::Data& currentDiff = differences[i];
            Difference::Data dd;

            #pragma omp critical
            dd = applyInvLinearTransform(std::move(currentDiff), diffPos);
            Difference diff(dd);

            // Устанавливаем все уже известные (ранее вычисленные) столбцы
            for (size_t pos = 0; pos < LSX::state_size_default; ++pos) {
                const size_t actualPos = calcNewPos(currentRound + 1, pos);
                
                if (context[pos].size() > 0) {
                    size_t ind = std::get<1>(context[pos].front());
                    std::list<uint8_t> knownBytes = splitByBytes(ind, depth);
                    auto knownIt = knownBytes.begin();
                    std::advance(knownIt, currentRound);

                    for (size_t j = 0; j < diff.get_size(); ++j)
                        diff.get_row(j)[actualPos] = sbox[ *knownIt ^ dd.data[j][actualPos] ];
                }
            }
            diff.get_anchor() = diff.get_row(0);

            for (size_t j = 0; j < MAX_BYTE; ++j) {
                Difference::Data dd = calcOneDifference(diff, diffPos, j, sbox);

                #pragma omp critical
                result.insert(j + i * differences.size(), std::move(dd));
            }
        }

        if (depth == currentRound + 1)
            return result;
        else
            return fill(depth, diffPosBasic, currentRound + 1, wrapReference(result), context);
    }

    Difference::Data applyInvLinearTransform(Difference::Data&& dd, size_t diffPos) const
    {
        for (size_t i = 0; i < dd.data.size(); ++i) {
            cipher_.get_state() = dd.data[i];
            InvLinearTransformCustom(diffPos);
            dd.data[i] = cipher_.get_state();
        }

        return dd;
    }

    void InvLinearTransformCustom(size_t diffPos) const
    {
        for (size_t i = 0; i < cipher_.get_linear_tacts(); ++i) {
            cipher_.InvLinearTransformTact();

            if ((diffPos + cipher_.get_linear_tacts() - i) % LSX::state_size_default != 0)
                cipher_.get_state()[15] = 0;
        }
    }

private:
    OrderedDifferenceDataContainer data_;
    LSX& cipher_;
    size_t diffPos_;
};


void Show256(OrderedDifferenceDataContainer::const_iterator it, int s)
{
  for (size_t i = 0; i < 256; ++i) {
    std::cout << "state[" << s << "] = " << std::hex << std::uppercase << std::setfill('0')
              << std::setw(2) << i << std::endl;
    Show(it->second);
    ++it;
    std::cout << std::endl;
  }
}

static Difference::Data calcOneDifference(const Difference& diff, size_t diffPos, uint8_t val, const SBox<uint8_t>& sbox)
{
    Difference::Data result = diff.FormData();

    result.base[diffPos] = sbox[val];
    for (size_t i = 1; i < diff.get_size(); ++i) {
        // фактическое значение
        result.data[i][diffPos] = sbox[ val ^ result.data[i][diffPos] ];

        // разность
        result.data[i][diffPos] ^= result.base[diffPos];
    }

    return result;
}

static OrderedDifferenceColumnContainer extractColumns(size_t diffPos,
            OrderedDifferenceDataContainer::const_iterator it,
            const OrderedDifferenceDataContainer::const_iterator& lastIt)
{
    OrderedDifferenceColumnContainer result;

    while (it != lastIt) {
        DifferenceColumn dc;
        const Difference::Data& dd = it->second;
        assert(dc.size() == dd.data.size());

        for (size_t i = 0; i < dc.size(); ++i)
            dc[i] = dd.data[i][diffPos];
        result.insert(it->first, std::move(dc));

        ++it;
    }

    return result;
}

static std::vector< std::reference_wrapper<Difference::Data> > wrapReference(const OrderedDifferenceDataContainer& src)
{
  std::vector< std::reference_wrapper<Difference::Data> > res;

  for (size_t i = 0; i < src.size(); ++i)
    res.emplace_back(std::ref(const_cast<Difference::Data&>(src[i])));

  return res;
}

#endif // BORDER_CROSSING_DIFFERENCES