#ifndef CONTAINER_CONCEPT_H
#define CONTAINER_CONCEPT_H

#include <concepts>
#include <iterator>

template <typename T>
concept Container = requires(T t) {
    typename T::value_type;   // T должен иметь value_type
    { std::begin(t) } -> std::input_iterator;
    { std::end(t) } -> std::input_iterator;
    { t.size() } -> std::same_as<typename T::size_type>;
};

#endif // CONTAINER_CONCEPT_H
