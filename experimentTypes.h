#ifndef EXPERIMENT_TYPES_H
#define EXPERIMENT_TYPES_H

#include <array>

#include "commonTypes.h"

const size_t MAX_BYTE = 256;
const size_t plaintextsCount = 6;

using DifferenceColumn = std::array<uint8_t, plaintextsCount>;

struct DifferenceColumnComparator {
    bool operator()(const DifferenceColumn& l, const DifferenceColumn& r) const
    {
        return std::lexicographical_compare(l.begin(), l.end(), r.begin(), r.end());
    }
};

struct DifferenceDataComparator {
    bool operator()(const Difference::Data& l, const Difference::Data& r) const
    {
        const Byte2Vector& ld = l.data;
        const Byte2Vector& rd = r.data;

        return std::lexicographical_compare(ld.begin(), ld.end(), rd.begin(), rd.end());
    }
};


#include "orderedContainer.h"
using OrderedDifferenceColumnContainer = OrderedContainer<DifferenceColumn, DifferenceColumnComparator>;
using OrderedDifferenceDataContainer = OrderedContainer<Difference::Data, DifferenceDataComparator>;

using oneByteCandidates = std::list<std::tuple<size_t, size_t, DifferenceColumn>>;

#endif // EXPERIMENT_TYPES_H
