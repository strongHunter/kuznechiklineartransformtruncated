/*
 * difference.cpp
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#include "difference.h"
#include "common.h"

Difference::Difference(size_t depth, size_t width)
{
  depth_ = depth;
  width_ = width;
  diff_ = Byte2Vector(depth, ByteVector(width, 0));
  anchor_ = ByteVector(width, 0);
}

Difference::Difference(const Byte2Vector &data)
{
  depth_ = data.size() - 1;
  anchor_ = data[0];
  width_ = data[0].size();
  for (size_t i = 1; i < data.size(); ++i)
  {
    diff_.push_back(anchor_ ^ data[i]);
  }
}

Difference::Difference(const Data& data)
{
  depth_ = data.data.size();
  anchor_ = data.base;
  width_ = data.base.size();
  for (size_t i = 0; i < data.data.size(); ++i)
  {
    diff_.push_back(anchor_ ^ data.data[i]);
  }
}

Difference::Data Difference::FormData() const
{
  Byte2Vector data;
  for (size_t i = 0; i < diff_.size(); i++)
  {
    data.push_back(diff_[i] ^ anchor_);
  }
  return { anchor_, data };
}

ByteVector& Difference::get_anchor()
{
  return anchor_;
}

size_t Difference::get_size() const
{
  return depth_;
}

size_t Difference::get_width() const
{
  return width_;
}

ByteVector& Difference::get_row(size_t i)
{
  return diff_[i];
}

const ByteVector& Difference::get_row(size_t i) const
{
  return diff_[i];
}

ByteVector Difference::get_column(size_t column)
{
  ByteVector res;
  for (size_t i = 0; i < depth_; i++)
  {
    res.push_back(diff_[i][column]);
  }
  return res;
}

void Difference::set_column(const ByteVector &input, size_t j)
{
  for (size_t i = 0; i < depth_; i++)
  {
    diff_[i][j] = input[i];
  }
}

void Difference::Show()
{
  ::Show(diff_);
}
