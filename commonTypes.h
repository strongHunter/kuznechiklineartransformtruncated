#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H

#include <vector>
#include <cstdint>

using ByteVector = std::vector<uint8_t>;
using Byte2Vector = std::vector< std::vector<uint8_t> >;

inline bool operator<(const ByteVector& l, const ByteVector& r)
{
    return std::lexicographical_compare(l.begin(), l.end(), r.begin(), r.end());
}

inline bool operator<(const Byte2Vector& l, const Byte2Vector& r)
{
    return std::lexicographical_compare(l.begin(), l.end(), r.begin(), r.end());
}

#endif // COMMON_TYPES_H
