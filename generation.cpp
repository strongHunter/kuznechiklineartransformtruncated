/*
 * generation.cpp
 *
 *  Created on: Sep 28, 2018
 *      Author: sesve
 */

#include "generation.h"

std::random_device seed;
std::mt19937_64 rng(seed());
std::uniform_int_distribution<uint8_t> dst(0, 0xFF);

uint8_t GetRandom()
{
  return dst(rng);
}

void RandomVector(ByteVector &data)
{
  for (size_t i = 0; i < data.size(); i++)
  {
    data[i] = GetRandom();
  }
}

void ChoosenPlaintexts(Byte2Vector &plaintexts)
{
  RandomVector(plaintexts[0]);
  for (size_t i = 1; i < plaintexts.size(); i++)
  {
    plaintexts[i] = plaintexts[0];
    plaintexts[i][0] ^= i;
  }
}

void RandomPlaintexts(Byte2Vector &plaintexts)
{
  for (size_t i = 0; i < plaintexts.size(); i++)
  {
    RandomVector(plaintexts[i]);
  }
}

SBox<uint8_t> GenerateRandomS()
{
  SBox<uint8_t> random_s;
  for (int x = 0; x < 2 << (8 * sizeof(uint8_t)); x++)
  {
    random_s[x] = x;
  }

  for (int x = 2 << (8 * sizeof(uint8_t)) - 1; x > 0; x--)
  {
    auto rand_e = GetRandom() % x;
    std::swap(random_s[rand_e], random_s[x]);
  }
  return random_s;
}

Matrix<uint8_t> GenerateRandomM(int size, uint8_t poly)
{
  ByteVector random_v(size, 0);
  RandomVector(random_v);
  Matrix<uint8_t> rand_m(random_v, poly);
  return rand_m;
}

