/*
 * lsx.h
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#ifndef LSX_H_
#define LSX_H_

#include <iostream>
#include "common.h"
#include "containerConcept.h"
#include "sbox.h"
#include "matrix.h"

class LSX
{
  size_t state_size_;
  size_t rounds_;
  size_t linear_tacts_;
  SBox<uint8_t> sbox_;
  Matrix<uint8_t> linear_matrix_;
  ByteVector state_;
  Byte2Vector keys_;
  std::vector<Byte2Vector> lookup_tables_;
  std::vector<Byte2Vector> lookup_tables_inv_;
  SBox<uint8_t> invsbox_;
  Matrix<uint8_t> invlinear_matrix_;

  void FormLookupTables();
  
  template <Container container_t>
  void KeyScheduleBase(const container_t &master)
  {
    assert(rounds_ >= 1);
    const size_t roundKeysCount = static_cast<int>(rounds_) + 1 - 2;
    const size_t cCount = 4 * roundUp(roundKeysCount, 2);

    Byte2Vector C(cCount, ByteVector(state_size_, 0));
    for (int i = 0; i < cCount; i++)
    {
      C[i].back() += (i + 1);
      state_ = C[i];
      LinearTransform();
      C[i] = state_;
    }

    std::copy(master.begin(), master.begin() + KEY_SIZE/2, keys_[0].begin());
    std::copy(master.begin() + KEY_SIZE/2, master.end(), keys_[1].begin());

    ByteVector dub, l0, r0;
    l0 = keys_[0];
    r0 = keys_[1];

    for (int nk = 0; nk < cCount / 8; nk++)
    {
      for (int nc = 0; nc < 8; nc++)
      {
        dub = l0;
        LSXRound(l0, C[nc + 8*nk]);
        l0 = l0 ^ r0;
        r0 = dub;
      }
      keys_[2*nk+2] = l0;
      keys_[2*nk+3] = r0;
    }
  }

 public:
  static const size_t BLOCK_SIZE = 16;
  static const size_t KEY_SIZE = 32;

  static const size_t state_size_default = 16;
  static const size_t rounds_default = 9;
  static const size_t linear_tacts_default = 16;

  static const uint8_t l_coeff[16];

  LSX(const unsigned int state_size, const unsigned int rounds, size_t linear_tacts,
      const SBox<uint8_t>& sbox, const Matrix<uint8_t>& linear_matrix)
      : state_size_(state_size),
        rounds_(rounds),
        linear_tacts_(linear_tacts),
        sbox_(sbox),
        linear_matrix_(linear_matrix),
        state_(ByteVector(state_size, 0)),
        keys_(Byte2Vector(roundUp(rounds + 1, 2), ByteVector(state_size, 0))),
        lookup_tables_(
            std::vector<Byte2Vector>(
                state_size, Byte2Vector(256, ByteVector(state_size, 0)))),
        lookup_tables_inv_(
            std::vector<Byte2Vector>(
                state_size, Byte2Vector(256, ByteVector(state_size, 0))))
  {
    invsbox_ = sbox_.GenerateInverse();
    invlinear_matrix_ = linear_matrix_.GenerateInverse();
    FormLookupTables();
  }

  LSX(const LSX& rhs)
      : state_size_(rhs.state_size_),
        rounds_(rhs.rounds_),
        linear_tacts_(rhs.linear_tacts_),
        sbox_(rhs.sbox_),
        linear_matrix_(rhs.linear_matrix_),
        state_(rhs.state_),
        keys_(rhs.keys_),
        lookup_tables_(rhs.lookup_tables_),
        lookup_tables_inv_(rhs.lookup_tables_inv_),
        invsbox_(rhs.invsbox_),
        invlinear_matrix_(rhs.invlinear_matrix_)
  {
  }

  ByteVector& get_state();
  Byte2Vector& get_keys();
  SBox<uint8_t>& get_sbox();
  SBox<uint8_t>& get_inv_sbox();
  const SBox<uint8_t>& get_sbox() const;
  SBox<uint8_t>& get_invsbox();
  Matrix<uint8_t>& get_linear_matrix();
  Matrix<uint8_t>& get_invlinear_matrix();
  size_t get_rounds() const;
  size_t get_linear_tacts() const;

  void AddRoundKey(const ByteVector &key);
  void SubBytes();
  void LinearTransform();
  void LinearTransformTact();
  void InvSubBytes();
  void InvLinearTransform();
  void InvLinearTransformTact();
  void LSXRound(const ByteVector &key);
  void LSXRound(ByteVector &input, const ByteVector &key);
  void InvLSXRound(const ByteVector &key);

  void Encrypt();
  void KeySchedule(const ByteVector &master);
  void KeyScheduleFull(const ByteVector &master);
  void KeySchedule(const std::array<uint8_t, KEY_SIZE> &master);
  void KeyScheduleFull(const std::array<uint8_t, KEY_SIZE> &master);

  void Show();
};

#endif /* LSX_H_ */
