/*
 * lsx.cpp
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#include "lsx.h"
#include "common.h"

const uint8_t LSX::l_coeff[16] = {
  148, 32, 133, 16,
    194, 192, 1, 251, 1, 192,
  194, 16, 133, 32, 148, 1
};

static inline uint8_t calcL(const ByteVector& r) noexcept;


void LSX::FormLookupTables()
{
  for (size_t num_of_table = 0; num_of_table < state_size_; num_of_table++)
  {
    for (int x = 0; x < 256; x++)
    {
      for (size_t pos = 0; pos < state_size_; pos++)
      {
        lookup_tables_[num_of_table][x][pos] =
            linear_matrix_.mult_tables_[num_of_table * state_size_ + pos][x];
        lookup_tables_inv_[num_of_table][x][pos] = invlinear_matrix_
            .mult_tables_[num_of_table * state_size_ + pos][x];
      }
    }
  }
}

SBox<uint8_t>& LSX::get_sbox()
{
  return sbox_;
}

SBox<uint8_t>& LSX::get_inv_sbox()
{
  return invsbox_;
}

const SBox<uint8_t>& LSX::get_sbox() const
{
  return sbox_;
}

SBox<uint8_t>& LSX::get_invsbox()
{
  return invsbox_;
}

Matrix<uint8_t>& LSX::get_linear_matrix()
{
  return linear_matrix_;
}

Matrix<uint8_t>& LSX::get_invlinear_matrix()
{
  return invlinear_matrix_;
}

ByteVector& LSX::get_state()
{
  return state_;
}

Byte2Vector& LSX::get_keys()
{
  return keys_;
}

size_t LSX::get_rounds() const
{
  return rounds_;
}

size_t LSX::get_linear_tacts() const
{
  return linear_tacts_;
}

void LSX::AddRoundKey(const ByteVector &key)
{
  for (size_t i = 0; i < state_size_; i++)
  {
    state_[i] ^= key[i];
  }
}

void LSX::SubBytes()
{
  for (size_t i = 0; i < state_size_; i++)
  {
    state_[i] = sbox_[state_[i]];
  }
}

void LSX::LinearTransform()
{
  for (size_t i = 0; i < linear_tacts_; i++)
    LinearTransformTact();
}

void LSX::LinearTransformTact()
{
  uint8_t l = calcL(state_);
  memmove(&state_[1], &state_[0], 15);
  state_[0] = l;
}

void LSX::InvSubBytes()
{
  for (size_t i = 0; i < state_size_; i++)
  {
    state_[i] = invsbox_[state_[i]];
  }
}

void LSX::InvLinearTransform()
{
  for (size_t i = 0; i < linear_tacts_; i++)
    InvLinearTransformTact();
}

void LSX::InvLinearTransformTact()
{
  uint8_t l = state_[0];
  memmove(&state_[0], &state_[1], 15);
  state_[15] = l;
  l = calcL(state_);
  state_[15] = l;
}

void LSX::LSXRound(const ByteVector &key)
{
  AddRoundKey(key);
  SubBytes();
  LinearTransform();
}

void LSX::LSXRound(ByteVector &input, const ByteVector &key)
{
  for (size_t i = 0; i < input.size(); i++)
  {
    input[i] ^= key[i];
    input[i] = sbox_[input[i]];
  }
  ByteVector tmp(state_size_, 0);
  for (size_t i = 0; i < state_size_; i++)
  {
    tmp = lookup_tables_[i][input[i]] ^ tmp;
  }
  input = tmp;
}

void LSX::InvLSXRound(const ByteVector &key)
{
  AddRoundKey(key);
  InvLinearTransform();
  InvSubBytes();
}

void LSX::Encrypt()
{
  for (size_t round = 0; round < rounds_; ++round)
    LSXRound(keys_[round]);
  AddRoundKey(keys_[rounds_]);
}

void LSX::KeySchedule(const ByteVector &master)
{
  KeyScheduleBase<ByteVector>(master);
}

void LSX::KeyScheduleFull(const ByteVector &master)
{
  LSX cipher(state_size_default, rounds_default, linear_tacts_default, sbox_, linear_matrix_);
  cipher.KeySchedule(master);

  keys_ = std::move(cipher.keys_);
}

void LSX::KeySchedule(const std::array<uint8_t, KEY_SIZE> &master)
{
  KeyScheduleBase<std::array<uint8_t, KEY_SIZE>>(master);
}

void LSX::KeyScheduleFull(const std::array<uint8_t, KEY_SIZE> &master)
{
  LSX cipher(state_size_default, rounds_default, linear_tacts_default, sbox_, linear_matrix_);
  cipher.KeySchedule(master);

  keys_ = std::move(cipher.keys_);
}

void LSX::Show()
{
  for (size_t i = 0; i < state_size_; i++)
  {
    std::cout << std::hex << std::setfill('0') << std::setw(2)
              << static_cast<int>(state_[i]);
  }
  std::cout << std::endl;
}

static inline uint8_t mul_l_coeff(const ByteVector& r, size_t i) noexcept
{
    return mul_GF256(r[i], LSX::l_coeff[i]);
}

static inline uint8_t calcL(const ByteVector& r) noexcept
{
    return r[15]
        ^ mul_l_coeff(r, 14) ^ mul_l_coeff(r, 13) ^ mul_l_coeff(r, 12)
        ^ mul_l_coeff(r, 11) ^ mul_l_coeff(r, 10) ^ mul_l_coeff(r, 9)
        ^ r[8]
        ^ mul_l_coeff(r, 7)
        ^ r[6]
        ^ mul_l_coeff(r, 5)
        ^ mul_l_coeff(r, 4) ^ mul_l_coeff(r, 3) ^ mul_l_coeff(r, 2)
        ^ mul_l_coeff(r, 1) ^ mul_l_coeff(r, 0);
}
