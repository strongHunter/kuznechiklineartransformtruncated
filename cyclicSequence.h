#ifndef CYCLIC_SEQUENCE_H
#define CYCLIC_SEQUENCE_H

#include <vector>

template <typename T>
class CyclicSequence {
public:
    CyclicSequence(const std::vector<T>& elements)
        : elements_(elements), current_(0)
    {}

    const T& next()
    {
        const T& retval = elements_[current_++];
        if (current_ == elements_.size())
            current_ = 0;

        return retval;
    }

private:
    std::vector<T> elements_;
    size_t current_;
};

#endif // CYCLIC_SEQUENCE_H
