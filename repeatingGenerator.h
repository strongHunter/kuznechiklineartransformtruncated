#ifndef REPEATING_GENERATOR_H
#define REPEATING_GENERATOR_H

#include <cstdint>

// Генерирует последовательность вида:
// 0, 0, ... 0, 1, 1, ... 1, ... 255, 255, ... 255
// Все числа повторяются n (передается в конструктор) раз
class RepeatingByteSequenceGenerator {
public:
    RepeatingByteSequenceGenerator(size_t n, uint8_t init)
        : step_(n), counter_(0), result_(init)
    {}

    uint8_t nextVal()
    {
        if (++counter_ == step_) {
            counter_ = 0;
            return result_++;
        } else {
            return result_;
        }
    }

private:
    const size_t step_;
    size_t counter_;
    uint8_t result_;
};

#endif // REPEATING_GENERATOR_H
