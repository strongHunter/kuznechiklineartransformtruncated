/*
 * common.cpp
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#include "common.h"

void Show(const Byte2Vector &a)
{
  for (size_t i = 0; i < a.size(); i++)
    Show(a[i]);
}

void Show(const ByteVector &a)
{
  Show<ByteVector>(a);
}

void Show(const Difference::Data &d)
{
  Show<ByteVector>(d.base);

  for (size_t i = 0; i < d.data.size(); i++)
    Show<ByteVector>(d.data[i], "   ");
}

void Show(const DifferenceColumn& a)
{
  for (size_t i = 0; i < a.size(); ++i)
    std::cout << std::hex << std::uppercase << std::setfill('0')
              << std::setw(2) << static_cast<uint32_t>(a[i]) << std::endl;
}

const ByteVector operator^(const ByteVector& left, const ByteVector& right)
{
  ByteVector c(left.size(),0);
  for (size_t i = 0; i < left.size(); ++i)
    c[i] = left[i] ^ right[i];
  return c;
}

const DifferenceColumn operator^(const DifferenceColumn& left, const DifferenceColumn& right)
{
  DifferenceColumn result;
  for (size_t i = 0; i < result.size(); ++i)
    result[i] = left[i] ^ right[i];
  return result;
}

uint8_t mul_GF256(uint8_t x, uint8_t y) noexcept
{
	uint8_t z;
	
	z = 0;
	while (y) {		
		if (y & 1)
			z ^= x;
		x = (x << 1) ^ (x & 0x80 ? 0xC3 : 0x00);
		y >>= 1;
	}
		
	return z;
}

size_t roundUp(size_t a, size_t divider) noexcept
{
    return (a + divider - 1) / divider * divider;
}

DifferenceColumn extractColumn(const Difference::Data& d, size_t pos)
{
  DifferenceColumn result;

  assert(result.size() == d.data.size());
  for (size_t i = 0; i < d.data.size(); ++i)
    result[i] = d.data[i][pos];

  return result;
}

std::list<uint8_t> splitByBytes(size_t val, size_t count)
{
  assert(count < sizeof(val));
  std::list<uint8_t> result;
  const uint8_t* p = reinterpret_cast<uint8_t*>(&val);

  for (size_t i = 0; i < count; ++i)
    result.push_back(*p++);

  return result;
}
