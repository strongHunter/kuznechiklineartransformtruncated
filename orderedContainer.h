#ifndef ORDERED_CONTAINER
#define ORDERED_CONTAINER

#include <list>

#include <boost/bimap.hpp>
#include <boost/bimap/set_of.hpp>
#include <boost/bimap/multiset_of.hpp>

#include "containerConcept.h"

template <Container T, typename Comparator>
class OrderedContainer {
private:
    using container_t = boost::bimap <
        boost::bimaps::set_of<size_t>,
        boost::bimaps::multiset_of<T, Comparator>
    >;

public:
    using const_iterator = container_t::left_const_iterator;
    using iterator = container_t::left_iterator;

    void insert(size_t index, T&& data)
    {
        data_.insert( typename container_t::value_type(index, data) );
    }

    const T& operator[](size_t index) const
    {
        return data_.left.find(index)->second;
    }

    size_t size() const
    {
        return data_.size();
    }

    const_iterator find(const T& key) const
    {
        auto rightIt = data_.right.find(key);

        if (rightIt != data_.right.end())
            return data_.project_left(rightIt);
        else
            return data_.left.end();
    }

    iterator begin()
    {
        return data_.left.begin();
    }

    iterator end()
    {
        return data_.left.end();
    }

    const_iterator cbegin() const
    {
        return data_.left.begin();
    }

    const_iterator cend() const
    {
        return data_.left.end();
    }

    OrderedContainer transform(
            const std::function<std::pair<size_t, T>(size_t, const T&)>& func) const
    {
        OrderedContainer result;

        for (auto it = cbegin(); it != cend(); ++it) {
            auto [index, dc] = func(it->first , it->second);
            result.insert(index, std::move(dc));
        }

        return result;
    }

    // std::tuple
    //   size_t:    this index
    //   size_t:    other index
    //   T:         idential element
    std::list<std::tuple<size_t, size_t, T>> findIntersection(const OrderedContainer& other) const
    {
        std::list<std::tuple<size_t, size_t, T>> result;

        containers_intersection(
            data_, other.data_,
            std::back_inserter(result),
            Comparator{}
        );

        return result;
    }

    template <typename OutputIterator>
        requires std::output_iterator<OutputIterator, std::tuple<size_t, size_t, T>>
    static void containers_intersection(const container_t& first, const container_t second,
                            OutputIterator outIt, const Comparator& comp)
    {
        auto it1 = first.right.begin();
        auto it2 = second.right.begin();

        while (it1 != first.right.end() && it2 != second.right.end()) {
            if (comp(it1->first, it2->first)) {
                ++it1;
            } else {
                if (!comp(it2->first, it1->first)) {
                    std::tuple<size_t, size_t, T> el = { it1->second, it2->second, it1->first };
                    *outIt++ = std::move(el);
                    ++it1;
                }
                ++it2;
            }
        }
    }

private:
    container_t data_;
};

#endif // ORDERED_CONTAINER
