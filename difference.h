/*
 * difference.h
 *
 *  Created on: Sep 27, 2018
 *      Author: sesve
 */

#ifndef DIFFERENCE_H_
#define DIFFERENCE_H_

#include "commonTypes.h"

class Difference
{
  size_t depth_;
  size_t width_;
  Byte2Vector diff_;
  ByteVector anchor_;

 public:
  struct Data {
    ByteVector base;
    Byte2Vector data;

    using value_type = Byte2Vector;
    using size_type = size_t;
    using iterator = Byte2Vector::iterator;
    
    iterator begin()
    {
      return data.begin();
    }

    iterator end()
    {
      return data.end();
    }

    size_type size() const
    {
      return data.size();
    }
  };

  Difference(size_t depth, size_t width);
  Difference(const Byte2Vector& data);
  Difference(const Data& data);
  Data FormData() const;
  ByteVector& get_anchor();
  size_t get_size() const;
  size_t get_width() const;
  ByteVector& get_row(size_t i);
  const ByteVector& get_row(size_t i) const;
  ByteVector get_column(size_t column);
  void set_column(const ByteVector &input, size_t j);
  void Show();
};

#endif /* DIFFERENCE_H_ */
