#include <gtest/gtest.h>

#include "sbox.h"

TEST(SBox, Inverse)
{
  SBox<uint8_t> magma = {0x0C, 0x04, 0x6, 0x2, 0xA, 0x5, 0xB, 0x9, 0xE, 0x8,
      0xD, 0x7, 0x0, 0x3, 0xF, 0x1};
  SBox<uint8_t> inverse = magma.GenerateInverse();
  SBox<uint8_t> inv_correct = {12, 15, 3, 13, 1, 5, 2, 11, 9, 7, 4, 6, 0, 10, 8,
      14};
  for (size_t i = 0; i < 16; ++i)
  {
    ASSERT_EQ(inverse[i], inv_correct[i]);
  }
}
