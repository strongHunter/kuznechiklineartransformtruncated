#include <gtest/gtest.h>

#include "consts.h"
#include "lsx.h"


const ByteVector plainText = {
  0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x00,
  0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88
};

const ByteVector ethalonCT = {
  0x7f, 0x67, 0x9d, 0x90, 0xbe, 0xbc, 0x24, 0x30,
  0x5a, 0x46, 0x8d, 0x42, 0xb9, 0xd4, 0xed, 0xcd
};


TEST (LSX, KeySchedule)
{
  const size_t kStateSize = 16;
  const size_t kRounds = 9;
  const size_t linearTacts = 16;

  LSX kuznechik(kStateSize, kRounds, linearTacts, kuzn_sbox, kuzn_matrix);
  ByteVector masterKey(32);
  std::copy(kuzn_keys[0].begin(), kuzn_keys[0].end(), masterKey.begin());
  std::copy(kuzn_keys[1].begin(), kuzn_keys[1].end(), masterKey.begin() + 16);
  
  kuznechik.KeySchedule(masterKey);
  ASSERT_EQ(kuznechik.get_keys(), kuzn_keys);
}

TEST (LSX, KeyScheduleFull)
{
  const size_t kStateSize = 16;
  const size_t kRounds = 2;
  const size_t linearTacts = 1;

  LSX kuznechik(kStateSize, kRounds, linearTacts, kuzn_sbox, kuzn_matrix);
  ByteVector masterKey(32);
  std::copy(kuzn_keys[0].begin(), kuzn_keys[0].end(), masterKey.begin());
  std::copy(kuzn_keys[1].begin(), kuzn_keys[1].end(), masterKey.begin() + 16);
  
  kuznechik.KeyScheduleFull(masterKey);
  ASSERT_EQ(kuznechik.get_keys(), kuzn_keys);
}

TEST(LSX, KuznechikFull)
{
  const size_t kStateSize = 16;
  const size_t kRounds = 9;
  const size_t linearTacts = 16;
  
  const ByteVector xk = {
    0x99, 0xbb, 0x99, 0xff, 0x99, 0xbb, 0x99, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
  };

  const ByteVector sxk = {
    0xe8, 0x7d, 0xe8, 0xb6, 0xe8, 0x7d, 0xe8, 0xb6,
    0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6
  };

  const ByteVector lsxk = {
    0xe2, 0x97, 0xb6, 0x86, 0xe3, 0x55, 0xb0, 0xa1,
    0xcf, 0x4a, 0x2f, 0x92, 0x49, 0x14, 0x08, 0x30
  };

  LSX kuznechik(kStateSize, kRounds, linearTacts, kuzn_sbox, kuzn_matrix);
  kuznechik.get_keys() = kuzn_keys;
  kuznechik.get_state() = plainText;

  kuznechik.AddRoundKey(kuzn_keys[0]);
  ASSERT_EQ(kuznechik.get_state(), xk);

  kuznechik.SubBytes();
  ASSERT_EQ(kuznechik.get_state(), sxk);

  kuznechik.LinearTransform();
  ASSERT_EQ(kuznechik.get_state(), lsxk);

  for (int i = 1; i < kRounds; i++)
  {
    kuznechik.LSXRound(kuzn_keys[i]);
  }
  kuznechik.AddRoundKey(kuzn_keys[kRounds]);
  ASSERT_EQ(kuznechik.get_state(), ethalonCT);
}

TEST(LSX, KuznechikOneRound)
{
  const size_t kStateSize = 16;
  const size_t kRounds = 1;
  const size_t linearTacts = 16;

  const ByteVector xk = {
    0x99, 0xbb, 0x99, 0xff, 0x99, 0xbb, 0x99, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
  };

  const ByteVector sxk = {
    0xe8, 0x7d, 0xe8, 0xb6, 0xe8, 0x7d, 0xe8, 0xb6,
    0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6
  };

  const ByteVector lsxk = {
    0xe2, 0x97, 0xb6, 0x86, 0xe3, 0x55, 0xb0, 0xa1,
    0xcf, 0x4a, 0x2f, 0x92, 0x49, 0x14, 0x08, 0x30
  };
  
  LSX kuznechik(kStateSize, kRounds, linearTacts, kuzn_sbox, kuzn_matrix);
  kuznechik.get_keys() = kuzn_keys;
  kuznechik.get_state() = plainText;

  kuznechik.AddRoundKey(kuzn_keys[0]);
  ASSERT_EQ(kuznechik.get_state(), xk);

  kuznechik.SubBytes();
  ASSERT_EQ(kuznechik.get_state(), sxk);

  kuznechik.LinearTransform();
  ASSERT_EQ(kuznechik.get_state(), lsxk);
}

TEST(LSX, KuznechikOneRoundTruncatedL)
{
  const size_t kStateSize = 16;
  const size_t kRounds = 1;
  const size_t linearTacts = 1;

  const ByteVector xk = {
    0x99, 0xbb, 0x99, 0xff, 0x99, 0xbb, 0x99, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
  };

  const ByteVector sxk = {
    0xe8, 0x7d, 0xe8, 0xb6, 0xe8, 0x7d, 0xe8, 0xb6,
    0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6
  };

  const ByteVector lsxk = {
    0x30, 0xe8, 0x7d, 0xe8, 0xb6, 0xe8, 0x7d, 0xe8,
    0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6
  };
  
  LSX kuznechik(kStateSize, kRounds, linearTacts, kuzn_sbox, kuzn_matrix);
  kuznechik.get_keys() = kuzn_keys;
  kuznechik.get_state() = plainText;

  kuznechik.AddRoundKey(kuzn_keys[0]);
  ASSERT_EQ(kuznechik.get_state(), xk);

  kuznechik.SubBytes();
  ASSERT_EQ(kuznechik.get_state(), sxk);

  kuznechik.LinearTransform();
  ASSERT_EQ(kuznechik.get_state(), lsxk);
}

TEST(LSX, CheckLinearTransformMethodWithParameter)
{
  const size_t kStateSize = 16;
  const size_t kRounds = 9;
  const size_t linearTacts = 16;

  const ByteVector sxk = {
    0xe8, 0x7d, 0xe8, 0xb6, 0xe8, 0x7d, 0xe8, 0xb6,
    0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6, 0xb6
  };

  const ByteVector lsxk = {
    0xe2, 0x97, 0xb6, 0x86, 0xe3, 0x55, 0xb0, 0xa1,
    0xcf, 0x4a, 0x2f, 0x92, 0x49, 0x14, 0x08, 0x30
  };

  LSX kuznechik(kStateSize, kRounds, linearTacts, kuzn_sbox, kuzn_matrix);
  kuznechik.get_keys() = kuzn_keys;
  kuznechik.get_state() = sxk;

  kuznechik.LinearTransform();
  ASSERT_EQ(kuznechik.get_state(), lsxk);
}
