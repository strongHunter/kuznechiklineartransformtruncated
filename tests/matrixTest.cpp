#include <gtest/gtest.h>

#include "matrix.h"

TEST(Matrix, Inverse)
{
  Matrix<uint8_t> kuzn_matrix_r({0xcf, 0x6e, 0xa2, 0x76, 0x98, 0x20, 0xc8, 0x33,
                                    0x74, 0xc6, 0x87, 0x10, 0xbf, 0xda, 0x70,
                                    0x0c},
                                0xc3);
  auto inverse = kuzn_matrix_r.GenerateInverse();
  Matrix<uint8_t> kuzn_inv_matrix({170, 91, 236, 204, 152, 49, 58, 165, 223,
                                      169, 167, 67, 146, 193, 175, 83},
                                  0xc3);
  for (size_t i = 0; i < 16; ++i)
  {
    ASSERT_EQ(inverse[i], kuzn_inv_matrix[i]);
  }
}
