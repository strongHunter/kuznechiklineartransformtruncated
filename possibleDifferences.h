#ifndef POSSIBLE_DIFFERENCES_H
#define POSSIBLE_DIFFERENCES_H

#include <omp.h>

#include "common.h"
#include "experimentTypes.h"
#include "repeatingGenerator.h"
#include "sbox.h"
#include "cyclicSequence.h"

static Difference::Data calcOneDifference(size_t stateSize, size_t diffPos, const DifferenceColumn& dc,
                                            uint8_t value, const SBox<uint8_t>& sbox);
static std::vector< std::reference_wrapper<DifferenceColumn> > wrapReference(const OrderedDifferenceColumnContainer& src);


class PossibleDifferences {
public:
    PossibleDifferences(size_t depth, const DifferenceColumn& difference, const SBox<uint8_t>& sbox)
        : sbox_(sbox)
    {
        data_ = fill(depth, { const_cast<DifferenceColumn&>(difference) });
    }

    const DifferenceColumn& get(size_t index) const
    {
        return data_[index];
    }

    const OrderedDifferenceColumnContainer& getData() const
    {
        return data_;
    }

    size_t getDepth() const
    {
        double depth = std::log(data_.size()) / std::log(256);
        return static_cast<size_t>(depth);
    }

    std::vector<DifferenceColumn> toVector() const
    {
        std::vector<DifferenceColumn> result(data_.size());
        size_t i = 0;

        for (auto it = data_.cbegin(); it != data_.cend(); ++it)
            result[i++] = it->second;
        return result;
    }

protected:
    OrderedDifferenceColumnContainer fill(size_t depth, const std::vector< std::reference_wrapper<DifferenceColumn> >& differences)
    {
        OrderedDifferenceColumnContainer result;
        CyclicSequence<std::reference_wrapper<DifferenceColumn>> cyclicDifference(differences);

        std::vector<DifferenceColumn> dcArray(differences.size());
        for (size_t i = 0; i < dcArray.size(); ++i)
            dcArray[i] = cyclicDifference.next();

        size_t i;
        #pragma omp parallel for private(i)
        #pragma omp shared(result)
        for (i = 0; i < differences.size(); ++i) {
            const size_t generatorStep = differences.size();
            const uint8_t genInit = generatorStep >= MAX_BYTE ? i / (generatorStep / MAX_BYTE) : 0;
            RepeatingByteSequenceGenerator generator(generatorStep, genInit);

            auto dcIt = dcArray.cbegin();
            std::advance(dcIt, i * MAX_BYTE % dcArray.size());

            for (size_t j = 0; j < MAX_BYTE; ++j) {
                const size_t pos = 0;
                Difference::Data dd = calcOneDifference(1, pos, *dcIt, generator.nextVal(), sbox_);

                if (++dcIt == dcArray.cend())
                    dcIt = dcArray.cbegin();

                #pragma omp critical
                result.insert(j + i * MAX_BYTE, extractColumn(dd, pos));
            }
        }

        if (depth == 1)
            return result;
        else
            return fill(depth - 1, wrapReference(result));
    }

private:
    OrderedDifferenceColumnContainer data_;
    const SBox<uint8_t>& sbox_;
};


void Show256(OrderedDifferenceColumnContainer::const_iterator it, int s)
{
  for (size_t i = 0; i < 256; ++i) {
    std::cout << "state[" << s << "] = " << std::hex << std::uppercase << std::setfill('0')
              << std::setw(2) << i << std::endl;
    Show(it->second);
    ++it;
    std::cout << std::endl;
  }
}

static Difference::Data calcOneDifference(size_t stateSize, size_t diffPos, const DifferenceColumn& dc,
                                            uint8_t value, const SBox<uint8_t>& sbox)
{
    ByteVector state(stateSize, 0);
    Difference difference(dc.size(), stateSize);

    for (size_t j = 0; j < difference.get_size(); ++j) {
        state[diffPos] = value;
        state[diffPos] ^= dc[j];
        state[diffPos] = sbox[state[diffPos]];

        difference.get_row(j) = state;
    }

    difference.get_anchor() = difference.get_row(0);
    return difference.FormData();
}

static std::vector< std::reference_wrapper<DifferenceColumn> > wrapReference(const OrderedDifferenceColumnContainer& src)
{
  std::vector< std::reference_wrapper<DifferenceColumn> > res;

  for (size_t i = 0; i < src.size(); ++i)
    res.emplace_back(std::ref(const_cast<DifferenceColumn&>(src[i])));

  return res;
}

#endif // POSSIBLE_DIFFERENCES_H
