/*
 * Sergey Svetlov, 2019
 * JSC "InfoTeCS", Moscow, Russia
 */

#include <cassert>
#include <omp.h>

#include "borderCrossingDifferences.h"
#include "experiment.h"
#include "experimentTypes.h"
#include "matrix.h"
#include "sbox.h"
#include "lsx.h"
#include "common.h"
#include "repeatingGenerator.h"
#include "possibleDifferences.h"

static size_t countNonEmptyElements(const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates);
static ByteVector restoreMasterKey(const ByteVector& rk1, const ByteVector& rk2);

static std::pair<Difference, Difference> makeDiffs(const std::pair<std::vector<ByteVector>, std::vector<ByteVector>>& data);
static Difference makeDiff(const std::vector<ByteVector>& data);
static std::list<size_t> calcIndices(size_t begin, size_t end);
static std::pair<size_t, size_t> calcDimensions(const LSX& cipher);

static void transformFrontTupleElements(size_t dim,
                                        std::list<std::tuple<size_t, size_t, DifferenceColumn>>& data);
                                        
static void transformSecondTupleElements(size_t dim,
                                        std::list<std::tuple<size_t, size_t, DifferenceColumn>>& data);
static size_t reversed(size_t val, size_t count);

#ifdef DEBUG
void encryptionDetailedOutput(const LSX& baseCipher, const std::vector<ByteVector>& plainTextList);
#endif


Experiment::Experiment(const LSX& cipher, const ByteVector& masterKey)
    : cipher_(cipher), masterKey_(masterKey)
{
}

void Experiment::ShowParams()
{
    std::cout << "Rounds : " << cipher_.get_rounds() << std::endl;
    std::cout << "Linear Tacts : " << cipher_.get_linear_tacts() << std::endl;
    std::cout << "Master key : ";

    Show(masterKey_);

    std::cout << "Round keys : " << std::endl;
    auto keys = cipher_.get_keys();
    for (size_t i = 0; i < keys.size(); i++) {
        std::cout << "Key " << std::dec << i + 1 << " : ";
        Show(keys[i]);
    }
}

void Experiment::Run()
{
    const ByteVector plainText = {
      0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x01,
      0xf0, 0xe0, 0xd0, 0xc0, 0xb0, 0xa0, 0x90, 0x80
    };

    cipher_.KeyScheduleFull(masterKey_);

    //Get ciphertexts
    ShowParams();
    std::cout << "----------------------------------------------" << std::endl;
    std::cout << std::endl;

    auto [dimensionForward, dimensionBackward] = calcDimensions(cipher_);

    std::array<oneByteCandidates, LSX::BLOCK_SIZE> keyCandidates;
    size_t s1FoundCount, s2FoundCount, s3FoundCount;

    auto s1Diffs = stage1(keyCandidates, plainText);
    s1FoundCount = countNonEmptyElements(keyCandidates);

    auto s2Diffs = stage2(keyCandidates, plainText);
    s2FoundCount = countNonEmptyElements(keyCandidates) - s1FoundCount;

    auto s3Diffs = stage3(keyCandidates, plainText);
    s3FoundCount = countNonEmptyElements(keyCandidates) - s1FoundCount - s2FoundCount;

    size_t s1TextsCount = s1FoundCount > 0 ? s1Diffs.first.get_size() : 0;
    size_t s2TextsCount = s2FoundCount > 0 ? s2Diffs.first.get_size() : 0;
    size_t s3TextsCount = s3FoundCount > 0 ? s3Diffs.size() * s3Diffs.front().first.get_size() : 0;

    std::cout << "Stage 1. Found " << std::dec << s1FoundCount << " bytes" << std::endl;
    std::cout << "Stage 2. Found " << std::dec << s2FoundCount << " bytes" << std::endl;
    std::cout << "Stage 3. Found " << std::dec << s3FoundCount << " bytes" << std::endl;
    std::cout << std::endl;

    std::cout << "Stage 1: " << std::dec << s1TextsCount << " texts used" << std::endl;
    std::cout << "Stage 2: " << std::dec << s2TextsCount << " texts used" << std::endl;
    std::cout << "Stage 3: " << std::dec << s3TextsCount << " texts used" << std::endl;

    std::cout << "Total: " << std::dec << s1TextsCount + s2TextsCount + s3TextsCount
            << " texts used" << std::endl << std::endl;

    // std::list<std::pair<Difference, Difference>> texts = { s1Diffs, s2Diffs };
    // texts.splice(texts.end(), s3Diffs);

    // for (const auto& diffs : texts) {
    //     std::cout << "PT diffs:" << std::endl;
    //     Show(diffs.first.FormData());
    //     std::cout << std::endl;

    //     std::cout << "CT diffs:" << std::endl;
    //     Show(diffs.second.FormData());
    //     std::cout << std::endl;
    // }

    for (size_t diffPos = 0; diffPos < keyCandidates.size(); ++diffPos) {
        for (const auto& [ind1, ind2, dc] : keyCandidates[diffPos]) {
            std::list<uint8_t> stateByteChainForward, stateByteChainBackward;

            stateByteChainForward = splitByBytes(ind1, dimensionForward);
            stateByteChainBackward = splitByBytes(ind2, dimensionBackward);

            SBox<uint8_t>& inv_sbox = cipher_.get_invsbox();

            std::cout << "Pos: " << diffPos << " - Key path: ";
            for (const auto& byte : stateByteChainForward)
                std::cout << std::hex << (int)byte << " -> ";
            for (auto it = stateByteChainBackward.crbegin(); it != stateByteChainBackward.crend(); ++it)
                std::cout << std::hex << (int)inv_sbox[*it] << " -> ";
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;

    ByteVector rk1 = restoreRoundKey1(keyCandidates, plainText);
    ByteVector rk2 = restoreRoundKey2(keyCandidates, plainText, rk1);
    ByteVector masterKey = restoreMasterKey(rk1, rk2);

    if (std::equal(masterKey.cbegin(), masterKey.cend(), masterKey_.cbegin())) {
        std::cout << "Master key restored:" << std::endl;
        Show(masterKey);
    } else {
        std::cout << "Error in restoring key" << std::endl;
    }

// #ifdef DEBUG
//     encryptionDetailedOutput(cipher_, plainTextList);
// #endif
}

ByteVector Experiment::restoreRoundKey1(const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates,
                                        const ByteVector& plainText)
{
    ByteVector rk1(16, 0);
    auto [dimensionForward, dimensionBackward] = calcDimensions(cipher_);

    for (size_t diffPos = 0; diffPos < keyCandidates.size(); ++diffPos) {
        assert(keyCandidates[diffPos].size() == 1);

        const auto& [ind1, ind2, dc] = keyCandidates[diffPos].front();
        std::list<uint8_t> stateByteChainForward;
        stateByteChainForward = splitByBytes(ind1, dimensionForward);

        rk1[diffPos] = stateByteChainForward.front() ^ plainText[diffPos];
    }

    return rk1;
}

ByteVector Experiment::restoreRoundKey2(const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates,
                                        const ByteVector& plainText, const ByteVector& rk1)
{
    ByteVector rk2(16, 0);
    auto [dimensionForward, dimensionBackward] = calcDimensions(cipher_);

    // Round 1
    cipher_.get_state() = plainText ^ rk1;
    cipher_.SubBytes();
    cipher_.LinearTransform();
    ByteVector r1State = cipher_.get_state();

    for (size_t diffPos = 0; diffPos < keyCandidates.size(); ++diffPos) {
        assert(keyCandidates[diffPos].size() == 1);

        uint8_t byteFromChain;
        const auto& [ind1, ind2, dc] = keyCandidates[diffPos].front();

        if (dimensionForward > 1) {
            std::list<uint8_t> stateByteChainForward;
            stateByteChainForward = splitByBytes(ind1, dimensionForward);

            auto it = stateByteChainForward.cbegin();
            ++it;
            byteFromChain = *it;
        } else {
            std::list<uint8_t> stateByteChainBackward;
            stateByteChainBackward = splitByBytes(ind2, dimensionBackward);
            
            SBox<uint8_t>& inv_sbox = cipher_.get_invsbox();
            byteFromChain = inv_sbox[stateByteChainBackward.back()];
        }

        const size_t pos = (diffPos + cipher_.get_linear_tacts()) % LSX::state_size_default;
        rk2[pos] = byteFromChain ^ r1State[pos];
    }

    return rk2;
}

static size_t countNonEmptyElements(const std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates)
{
    size_t result = 0;

    for (size_t i = 0; i < keyCandidates.size(); ++i)
        if (keyCandidates[i].size() > 0)
            ++result;
    return result; 
}

static ByteVector restoreMasterKey(const ByteVector& rk1, const ByteVector& rk2)
{
    ByteVector masterKey(32, 0);

    std::copy(rk1.begin(), rk1.end(), masterKey.begin());
    std::copy(rk2.begin(), rk2.end(), masterKey.begin() + rk1.size());

    return masterKey;
}

static std::pair<std::vector<ByteVector>, std::vector<ByteVector>> formDataStage1(const ByteVector& plainText, LSX& cipher);
static std::pair<std::vector<ByteVector>, std::vector<ByteVector>> formDataStage2(const ByteVector& plainText, LSX& cipher);
static std::pair<std::vector<ByteVector>, std::vector<ByteVector>> formDataStage3(const ByteVector& plainText, LSX& cipher, size_t diffPos);

std::pair<Difference, Difference> Experiment::stage1(
                std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates, const ByteVector& plainText)
{
    auto [ptDifference, ctDifference] = makeDiffs(formDataStage1(plainText, cipher_));

    const size_t lastStage1DiffPos = LSX::BLOCK_SIZE - cipher_.get_rounds() * cipher_.get_linear_tacts();
    auto [dimensionForward, dimensionBackward] = calcDimensions(cipher_);

    for (size_t diffPos = 0; diffPos < lastStage1DiffPos; ++diffPos) {
        DifferenceColumn ptDiffs = extractColumn(ptDifference.FormData(), diffPos);
        DifferenceColumn ctDiffs = extractColumn(ctDifference.FormData(), diffPos + cipher_.get_rounds() * cipher_.get_linear_tacts());

        PossibleDifferences pdForward(dimensionForward, ptDiffs, cipher_.get_sbox());
        PossibleDifferences pdBackward(dimensionBackward, ctDiffs, cipher_.get_inv_sbox());

        keyCandidates[diffPos] = pdForward.getData().findIntersection(pdBackward.getData());
    }

    return std::make_pair(std::move(ptDifference), std::move(ctDifference));
}

static std::pair<std::vector<ByteVector>, std::vector<ByteVector>> formDataStage1(const ByteVector& plainText, LSX& cipher)
{
    const size_t lastStage1DiffPos = LSX::BLOCK_SIZE - cipher.get_rounds() * cipher.get_linear_tacts();

    std::vector<ByteVector> plainTextList(plaintextsCount);
    std::vector<ByteVector> cipherTextList(plaintextsCount);

    for (size_t i = 0; i < plaintextsCount; ++i) {
        plainTextList[i] = plainText;
        for (size_t j = 0; j < lastStage1DiffPos; ++j)
            plainTextList[i][j] ^= i;

        cipher.get_state() = plainTextList[i];
        cipher.Encrypt();
        cipherTextList[i] = cipher.get_state();
    }

    return std::make_pair(plainTextList, cipherTextList);
}

std::pair<Difference, Difference> Experiment::stage2(
                std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates, const ByteVector& plainText)
{
    auto [ptDifference, ctDifference] = makeDiffs(formDataStage2(plainText, cipher_));

    const size_t stage2DiffPos = LSX::BLOCK_SIZE - 1;
    DifferenceColumn ptDiffs = extractColumn(ptDifference.FormData(), stage2DiffPos);
    DifferenceColumn ctDiffs = extractColumn(ctDifference.FormData(),
                                    (stage2DiffPos + cipher_.get_rounds() * cipher_.get_linear_tacts())
                                        % LSX::state_size_default);
    auto [dimensionForward, dimensionBackward] = calcDimensions(cipher_);

    PossibleDifferences pdForward(dimensionForward, ptDiffs, cipher_.get_sbox());
    PossibleDifferences pdBackward(dimensionBackward, ctDiffs, cipher_.get_inv_sbox());

    keyCandidates[stage2DiffPos] = pdForward.getData().findIntersection(pdBackward.getData());

    return std::make_pair(std::move(ptDifference), std::move(ctDifference));
}

static std::pair<std::vector<ByteVector>, std::vector<ByteVector>> formDataStage2(const ByteVector& plainText, LSX& cipher)
{
    const size_t stage2DiffPos = LSX::BLOCK_SIZE - 1;

    std::vector<ByteVector> plainTextList(plaintextsCount);
    std::vector<ByteVector> cipherTextList(plaintextsCount);

    for (size_t i = 0; i < plaintextsCount; ++i) {
        plainTextList[i] = plainText;
        plainTextList[i][stage2DiffPos] ^= i;

        cipher.get_state() = plainTextList[i];
        cipher.Encrypt();
        cipherTextList[i] = cipher.get_state();
    }
    
    return std::make_pair(plainTextList, cipherTextList);
}

static std::list<std::pair<Difference, Difference>> stage3Engine(std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates,
                                                        LSX& cipher, const ByteVector& plainText,
                                                        const std::list<size_t>& dpIndices);

std::list<std::pair<Difference, Difference>> Experiment::stage3(
                std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates, const ByteVector& plainText)
{
    std::list<std::pair<Difference, Difference>> retval;

    const size_t lastStage1DiffPos = LSX::BLOCK_SIZE - cipher_.get_rounds() * cipher_.get_linear_tacts();
    const size_t stage2DiffPos = LSX::BLOCK_SIZE - 1;

    const size_t beginDiffPos = lastStage1DiffPos;
    const size_t endDiffPos = stage2DiffPos - 1;

    std::list<size_t> dpIndices = calcIndices(beginDiffPos, endDiffPos);
    retval.splice(retval.end(),
        stage3Engine(keyCandidates, cipher_, plainText, dpIndices)
    );

    size_t indicesCount = dpIndices.size();
    size_t newIndicesCount = indicesCount;

    // Повторный проход по не найденным с ранее позициям
    do {
        indicesCount = newIndicesCount;

        dpIndices.erase(std::remove_if(dpIndices.begin(), dpIndices.end(), [&keyCandidates](size_t index) {
            return keyCandidates[index].size() > 0;
        }), dpIndices.end());

        newIndicesCount = dpIndices.size();
        stage3Engine(keyCandidates, cipher_, plainText, dpIndices);
    } while (newIndicesCount != indicesCount);

    return retval;
}

static std::list<std::pair<Difference, Difference>> stage3Engine(std::array<oneByteCandidates, LSX::BLOCK_SIZE>& keyCandidates,
                                                        LSX& cipher, const ByteVector& plainText,
                                                        const std::list<size_t>& dpIndices)
{
    std::list<std::pair<Difference, Difference>> retval;

    auto [dimensionForward, dimensionBackward] = calcDimensions(cipher);
    for (size_t diffPos : dpIndices) {
        auto [ptDifference, ctDifference] = makeDiffs(formDataStage3(plainText, cipher, diffPos));

        DifferenceColumn ptDiffs = extractColumn(ptDifference.FormData(), diffPos);
        DifferenceColumn ctDiffs = extractColumn(ctDifference.FormData(),
                                        (diffPos + cipher.get_rounds() * cipher.get_linear_tacts()) 
                                            % LSX::state_size_default);

        if (diffPos + dimensionForward * cipher.get_linear_tacts() >= LSX::state_size_default) {
            PossibleDifferences pdBackward(dimensionBackward, ctDiffs, cipher.get_inv_sbox());
            BorderCrossingDifferences bd(dimensionForward, ptDifference.FormData(), diffPos, cipher, keyCandidates);

            OrderedDifferenceColumnContainer ccBacward;
            ccBacward = bd.extractColumns();

            keyCandidates[diffPos] = ccBacward.findIntersection(pdBackward.getData());
            transformFrontTupleElements(dimensionForward, keyCandidates[diffPos]);
        } else {
            PossibleDifferences pdForward(dimensionForward, ptDiffs, cipher.get_sbox());
            InverseBorderCrossingDifferences ibd(dimensionBackward, ctDifference.FormData(), diffPos, cipher, keyCandidates);
            
            OrderedDifferenceColumnContainer ccBacward;
            ccBacward = ibd.extractColumns();

            keyCandidates[diffPos] = pdForward.getData().findIntersection(ccBacward);
            transformSecondTupleElements(dimensionBackward, keyCandidates[diffPos]);
        }

        retval.emplace_back(std::make_pair(std::move(ptDifference), std::move(ctDifference)));
    }
    return retval;
}

static std::list<size_t> calcIndices(size_t begin, size_t end)
{
    std::list<size_t> result;
    while (begin < end) {
        result.push_back(end);
        --end;
        if (end != begin) {
            result.push_back(begin);
            ++begin;
        }
    }
    assert(begin == end);
    result.push_back(begin);

    return result;
}

static std::pair<std::vector<ByteVector>, std::vector<ByteVector>> formDataStage3(const ByteVector& plainText, LSX& cipher, size_t diffPos)
{
    std::vector<ByteVector> plainTextList(plaintextsCount);
    std::vector<ByteVector> cipherTextList(plaintextsCount);

    for (size_t i = 0; i < plaintextsCount; ++i) {
        plainTextList[i] = plainText;
        plainTextList[i][diffPos] ^= i;

        cipher.get_state() = plainTextList[i];
        cipher.Encrypt();
        cipherTextList[i] = cipher.get_state();
    }

    return std::make_pair(plainTextList, cipherTextList);
}

static std::pair<size_t, size_t> calcDimensions(const LSX& cipher)
{
    size_t dimensionForward = (cipher.get_rounds() / 2) + ( (cipher.get_rounds() % 2) ? 1 : 0 );
    size_t dimensionBackward = cipher.get_rounds() - dimensionForward;

    return std::make_pair(dimensionForward, dimensionBackward);
}

static std::pair<Difference, Difference> makeDiffs(const std::pair<std::vector<ByteVector>, std::vector<ByteVector>>& data)
{
    return std::make_pair(makeDiff(data.first), makeDiff(data.second));
}

static Difference makeDiff(const std::vector<ByteVector>& data)
{
    Difference diff(plaintextsCount, data.front().size());
    for (size_t i = 0; i < plaintextsCount; ++i)
        diff.get_row(i) = data[i];
    diff.get_anchor() = diff.get_row(0);

    return diff;
}

static void transformFrontTupleElements(size_t dim,
                                        std::list<std::tuple<size_t, size_t, DifferenceColumn>>& data)
{
    for (auto& [ind1, ind2, dc] : data) {
        ind1 = reversed(ind1, dim);
    }
}

static void transformSecondTupleElements(size_t dim,
                                        std::list<std::tuple<size_t, size_t, DifferenceColumn>>& data)
{
    for (auto& [ind1, ind2, dc] : data) {
        ind2 = reversed(ind2, dim);
    }
}

static size_t reversed(size_t val, size_t count)
{
    uint8_t* lp = reinterpret_cast<uint8_t*>(&val);
    uint8_t* hp = lp + count - 1;

    while (lp < hp) {
        std::swap(*lp, *hp);
        ++lp;
        --hp;
    }

    return val;
}

#ifdef DEBUG
static void print(Difference& diff);

void encryptionDetailedOutput(const LSX& baseCipher, const std::vector<ByteVector>& plainTextList)
{
    const size_t count = plainTextList.size();
    const size_t rounds = baseCipher.get_rounds();

    std::vector<LSX> ciphersList(count, baseCipher);
    for (size_t i = 0; i < count; ++i) {
        ciphersList[i].get_state() = plainTextList[i];
    }

    Difference diff(count, plainTextList.front().size());

    std::cout << "Plaintext:" << std::endl;
    for (size_t i = 0; i < count; ++i) {
        LSX& cipher = ciphersList[i];

        diff.get_row(i) = cipher.get_state();
    }
    print(diff);

    for (size_t round = 0; round < rounds; ++round) {
        std::cout << "Round " << round + 1 << ":" << std::endl;

        std::cout << "Added round key:" << std::endl;
        for (size_t i = 0; i < count; ++i) {
            LSX& cipher = ciphersList[i];

            cipher.AddRoundKey(cipher.get_keys()[round]);
            diff.get_row(i) = cipher.get_state();
        }
        print(diff);

        std::cout << "SBox:" << std::endl;
        for (size_t i = 0; i < count; ++i) {
            LSX& cipher = ciphersList[i];

            cipher.SubBytes();
            diff.get_row(i) = cipher.get_state();
        }
        print(diff);

        std::cout << "Linear transform:" << std::endl;
        for (size_t i = 0; i < count; ++i) {
            LSX& cipher = ciphersList[i];
        
            cipher.LinearTransform();
            diff.get_row(i) = cipher.get_state();
        }
        print(diff);
        std::cout << std::endl;
    }

    std::cout << "Added round key:" << std::endl;
    for (size_t i = 0; i < count; ++i) {
        LSX& cipher = ciphersList[i];

        cipher.AddRoundKey(cipher.get_keys()[rounds]);
        diff.get_row(i) = cipher.get_state();
    }
    print(diff);
    std::cout << std::endl;
}

static void print(Difference& diff)
{
    diff.Show();
    std::cout << "------------------------------------------------ Diff:" << std::endl;
    diff.get_anchor() = diff.get_row(0);
    Show(diff.FormData());
}
#endif
