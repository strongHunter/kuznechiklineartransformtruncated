/*
 * geheration.h
 *
 *  Created on: Sep 28, 2018
 *      Author: sesve
 */

#ifndef GENERATION_H_
#define GENERATION_H_

#include <random>
#include "common.h"
#include "sbox.h"
#include "matrix.h"

uint8_t GetRandom();
void RandomVector(ByteVector &data);
void ChoosenPlaintexts(Byte2Vector &plaintexts);
void RandomPlaintexts(Byte2Vector &plaintexts);

SBox<uint8_t> GenerateRandomS();
Matrix<uint8_t> GenerateRandomM(int size, uint8_t poly);

#endif /* GENERATION_H_ */
