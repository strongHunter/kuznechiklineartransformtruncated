/*
 * Sergey Svetlov, 2019
 * JSC "InfoTeCS", Moscow, Russia  
 */

#include "consts.h"
#include "lsx.h"
#include "generation.h"
#include "experiment.h"

#include <chrono>

int main(int argc, char* argv[])
{
  const size_t kStateSize = 16;
  size_t kRounds;
  size_t linearTacts;

  if (argc != 5) {
    std::cerr << "Usage: " << argv[0] << " -r <num> -t <num>" << std::endl;
    return 1;
  }

  for (size_t i = 1; i < argc; i += 2) {
    if (std::string(argv[i]) == "-r") {
      kRounds = std::atoi(argv[i + 1]);
    } else if (std::string(argv[i]) == "-t") {
      linearTacts = std::atoi(argv[i + 1]);
    } else {
      std::cerr << "Unknown argument: " << argv[i] << std::endl;
      return 1;
    }
  }

  #ifdef WINDOWS
  srand(time(NULL));
  #endif

  ByteVector masterKey(LSX::KEY_SIZE, 0);
  RandomVector(masterKey);

  LSX cipher(kStateSize, kRounds, linearTacts, kuzn_sbox, kuzn_matrix);
  Experiment experiment(cipher, masterKey);

  auto start_time = std::chrono::steady_clock::now();
  experiment.Run();

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_s = std::chrono::duration_cast<std::chrono::seconds>(
      end_time - start_time);
  std::cout << std::dec << std::endl << "Experiment total time: "
            << elapsed_s.count() << " s" << std::endl;

  #ifdef WINDOWS
  std::cin.get();
  #endif

  return 0;
}
